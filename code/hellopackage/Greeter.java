package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter
{
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);
        // The reason to fully quantify a name this way, would be if it turned
        // out you had 2 classes with the same name BOTH imported into the same program!
        // java.util.Random ran = new java.util.Random();
        Random ran = new Random();
        System.out.println("Enter an int");
        int num = scan.nextInt();
        System.out.println(Utilities.doubleMe(num));
    }
}